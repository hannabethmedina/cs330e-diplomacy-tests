#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_print, diplomacy_eval, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):

    def test_war_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        s += "D Austin Move London"
        v = diplomacy_eval(s)
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_war_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A"
        v = diplomacy_eval(s)
        self.assertEqual(
            v, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_war_3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B"
        v = diplomacy_eval(s)
        self.assertEqual(
            v, "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A Madrid")
        self.assertEqual(w.getvalue(), "A Madrid")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "A Madrid Hold")
        self.assertEqual(w.getvalue(), "A Madrid Hold")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, "C London Hold")
        self.assertEqual(w.getvalue(), "C London Hold")

    def test_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()
